export class Factory {
    id: string
    correlation_id?: number
    created_at: Date
    name: string
    num_children: number
    lower_range: number
    upper_range: number
    children: [number]
}