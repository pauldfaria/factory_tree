export enum FromBrowser {
    New,
    Update,
    Refresh,
    Delete,
    Ping
}

export enum FromServer {
    Initial,
    Created,
    Updated,
    Deleted,
    Pong
}

export class Message {
    method: FromBrowser | FromServer
    data: any
}
