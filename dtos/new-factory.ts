export class NewFactory {
    correlation_id: string
    name: string
    num_children: number
    lower_range: number
    upper_range: number
}