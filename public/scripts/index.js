requirejs(['knockout', 'root-view-model'], function(ko, rvm) {
    var websocket_addr = location.origin.replace(/^http/, 'ws');
    var max_children = parseInt(document.getElementById('max_children').innerText.trim(), 10);

    var root_view_model = new rvm.RootViewModel(websocket_addr, max_children);
    ko.applyBindings(root_view_model);

    // Enable UI
    var remove_class = function (node, className) {
        node.className = node.className.replace(className.trim(), "");
    };
    remove_class(document.getElementById("add_factory"), "disable");
    remove_class(document.getElementById("factories"), "hide");
});