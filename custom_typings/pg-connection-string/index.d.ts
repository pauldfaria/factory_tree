declare namespace PgConnectionString {
    interface Config {
        host: string;
        database: string;
        application_name?: string;
        fallback_application_name?: string;
        port?: string;
        client_encoding?: string;
        user?: string;
        password?: string;
        ssl?: boolean;
    }

    function parse(connectionStr: string): Config;
}

export = PgConnectionString;
