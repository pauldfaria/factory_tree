import * as ko from "knockout";
import { FactoryViewModel } from "factory-view-model";
import { Factory } from "dtos/factory";
import { Message, FromBrowser, FromServer } from "dtos/message";
import { RefreshFactory } from "dtos/refresh-factory";
import { DeleteFactory } from "dtos/delete-factory";
declare var $: any;

export class RootViewModel {
    websocket: WebSocket
    max_children: number
    factories: KnockoutObservableArray<FactoryViewModel>
    pending_updates: KnockoutObservableArray<Factory>
    websocket_keepalive?: number
    bound_delete_factory: KnockoutObservable<Factory> | null
    bound_refresh_factory: KnockoutObservable<Factory> | null

    children_range: KnockoutComputed<number[]>
    sorted_factories: KnockoutComputed<FactoryViewModel[]>
    empty: KnockoutComputed<boolean>
    reconnecting: boolean

    constructor(
        websocket_address: string,
        max_children: number,
    ) {
        this.connect_websocket(websocket_address);
        this.max_children = max_children;
        this.factories = ko.observableArray<FactoryViewModel>();
        this.pending_updates = ko.observableArray<Factory>();
        this.reconnecting = false;

        this.children_range = ko.computed(() => {
            let range: number[] = [];
            for (let i = 1; i <= this.max_children; ++i) {
                range.push(i);
            }

            return range;
        });

        // This is what the list is bound to in the UI. While the
        // server initially returns us a sorted list, we have to account for
        // factories that are added from websocket updates, which may come
        // out of order. This won't be optimal if there end up being a very
        // large number of factories, but will suffice for now.
        this.sorted_factories = ko.computed(() => {
            return this.factories().sort((a, b) => {
                let x = a.created_at();
                let y = b.created_at();

                if (x.getTime() === new Date(0).getTime()) {
                    return 1;
                }

                if (y.getTime() === new Date(0).getTime()) {
                    return -1;
                }

                if (x > y) {
                    return 1;
                } else if (x < y) {
                    return -1;
                }

                return 0;
            });
        });

        this.empty = ko.computed(() => {
            return this.factories().length === 0;
        });

        // These help us keep track of which factory is bound
        // to a particular modal when they're open.
        this.bound_delete_factory = null;
        this.bound_refresh_factory = null;

        // Unbind primary buttons on modals when they close since
        // we bind the button to the specific instance which caused
        // it to open.
        $('#refresh-modal').on('hide.bs.modal', () => $('#refresh-button').off('click'));
        $('#delete-modal').on('hide.bs.modal', () => $('#delete-button').off('click'));
        $('#pending-modal').on('hide.bs.modal', () => $('#pending-button').off('click'));

        // Focus the cancel button when deleting and the primary buttons
        // for the other modals. This should ideally highlight the safer/preferred
        // action and should improve usability.
        $('#delete-modal').on('shown.bs.modal', () => $('#delete-cancel').focus());
        $('#refresh-modal').on('shown.bs.modal', () => $('#refresh-button').focus());
        $('#pending-modal').on('shown.bs.modal', () => $('#pending-button').focus());
    }

    // ui bound
    save_changes(factory_view_model: FactoryViewModel) {
        if (!factory_view_model.save_changes(this.websocket)) {
            return;
        }

        // If there was a pending change for this factory
        // then delete it. The user has already overwritten it.
        for (let i = 0; i < this.pending_updates().length; ++i) {
            let f = this.pending_updates()[i];
            if (f.id === factory_view_model.id()) {
                this.pending_updates.remove(f);
                break;
            }
        }
    }

    // ui bound
    cancel_changes(factory_view_model: FactoryViewModel) {
        // Delete the factory if it's never been saved
        if (!factory_view_model.id()) {
            this.factories.remove(factory_view_model);
            return;
        }

        // otherwise just cancel the changes
        factory_view_model.cancel_changes();

        // If there was a pending update, then apply it now.
        for (let i = 0; i < this.pending_updates().length; ++i) {
            let f = this.pending_updates()[i];
            if (f.id === factory_view_model.id()) {
                // We want to let the user know an update came in
                // so as not to confuse them when it doesn't match
                // the original values. An improved version of this
                // would ask them as they're editing it and provide
                // a conlfict resolution UI.
                $('#pending-button').on('click', () => {
                    this.update_factory(f, true);
                    this.pending_updates.remove(f);
                    $('#pending-modal').modal('hide');
                });
                $('#pending-modal').modal();
                break;
            }
        }
    }

    // ui bound
    add_factory() {
        // default, editable factory
        let factory_view_model = new FactoryViewModel(null);
        factory_view_model.edit();
        this.factories.push(factory_view_model);
    }

    handle_message(event: MessageEvent) {
        let message: Message = JSON.parse(event.data);
        switch (message.method) {
            case FromServer.Pong:
                // The server is just verifying that it received
                // our ping. We don't need to do anything currenly.
                // This is just to differentiate between known and
                // unknown messages.
                break;
            case FromServer.Initial: {
                let factories: [Factory] = message.data;

                // We may have old factories around from a previous connection.
                // Remove all the ones that are not being updated
                this.factories.remove(factory => !factory.is_edit());
                factories.forEach(factory => this.update_factory(factory));
                break;
            }
            case FromServer.Created: {
                // Replace the single, matched factory
                let i = 0;
                let factory = new FactoryViewModel(message.data);
                for (; i < this.factories().length; ++i) {
                    if (this.factories()[i].correlation_id() === message.data.correlation_id) {
                        this.factories.replace(this.factories()[i], factory);
                        break;
                    }
                }

                if (i === this.factories().length) {
                    // Someone else created this item, we need to add it
                    this.factories.push(factory);
                }
                break;
            }
            case FromServer.Updated:
                // Run update on all factories in the message
                this.update_factory(message.data);
                break;
            case FromServer.Deleted:
                // Remove any matched factory
                this.factories.remove((fvm) => fvm.id() === message.data);
                break;
            default:
                console.error("Unknown message from server: %o", message);
                break;
        }
    }

    update_factory(factory: Factory, replace_edit: boolean = false) {
        let factories = this.factories();
        let match = null;

        // Search for the matching factory in the list of current factories
        for (let i = 0; i < factories.length; ++i) {
            if (factories[i].id() === factory.id) {
                match = factories[i];
                break;
            }
        }

        if (match) {
            // We've found an existing entry, so attempt to update it.
            // If the update fails, save it to the pending_updates array
            // so we can determine what to do after feedback from the user.
            if (!match.update_from(factory, replace_edit)) {
                // Remove previous pending updates for this factory. The
                // old updates are no longer valid at this point.
                for (let i = 0; i < this.pending_updates().length; ++i) {
                    let f = this.pending_updates()[i];
                    if (f.id === factory.id) {
                        this.pending_updates.remove(f);
                    }
                }

                this.pending_updates.push(factory);
            }
        } else {
            // Otherwise we have a new factory that was added by some other
            // user. Just add it to the factories list.
            let factory_view_model = new FactoryViewModel(factory);
            this.factories.push(factory_view_model);
        }
    }

    // ui bound
    refresh(factory: FactoryViewModel) {
        $('#refresh-button').on('click', () => {
            let data: RefreshFactory = {
                refresh_id: factory.id()
            };
            this.websocket.send(JSON.stringify({
                method: FromBrowser.Refresh,
                data: data
            }));
            $('#refresh-modal').modal('hide');
        });
        $('#refresh-modal').modal();
    }

    // ui bound
    delete(factory: FactoryViewModel) {
        $('#delete-button').on('click', () => {
            let data: DeleteFactory = {
                delete_id: factory.id()
            };
            this.websocket.send(JSON.stringify({
                method: FromBrowser.Delete,
                data: data
            }));
            $('#delete-modal').modal('hide');
        });
        $('#delete-modal').modal();
    }

    connect_websocket(websocket_address: string) {
        this.websocket = new WebSocket(websocket_address, ["factory"]);
        this.websocket.onopen = (_ev: Event) => {
            this.reconnecting = false;
            console.info("Websocket connection succesful");
        };
        this.websocket.onclose = (ev: CloseEvent) => {
            if (this.websocket_keepalive) {
                window.clearInterval(this.websocket_keepalive);
                this.websocket_keepalive = null;
            }

            if (this.reconnecting) {
                window.setTimeout(() => {
                    this.reconnect_websocket(ev, websocket_address);
                }, 5000);
            } else {
                this.reconnecting = true;
                this.reconnect_websocket(ev, websocket_address);
            }
        }
        this.websocket.onmessage = this.handle_message.bind(this);
        this.websocket.onerror = (ev: ErrorEvent) => console.error("Error from websocket: %o", ev);
        this.websocket_keepalive = window.setInterval(() => {
            let message: Message = {
                method: FromBrowser.Ping,
                data: Date
            };
            this.websocket.send(JSON.stringify(message));
        }, 45000); // ping every 45 seconds to keep the heroku connection alive
    }

    reconnect_websocket(ev: CloseEvent, websocket_address: string) {
        console.info("Websocket closed by server: %o\nAttempting to reconnect...", ev);
        this.connect_websocket(websocket_address);
    }
}