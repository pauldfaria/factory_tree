import * as ko from "knockout";
import { Factory } from "dtos/factory";
import { EditableFactoryViewModel } from "editable-factory-view-model";
import { Message, FromBrowser } from "dtos/message";
import { NewFactory } from "dtos/new-factory";
import { UpdateFactory } from "dtos/update-factory";

export class FactoryViewModel {
    id: KnockoutObservable<string>
    correlation_id: KnockoutObservable<string>
    created_at: KnockoutObservable<Date>
    name: KnockoutObservable<string>
    num_children: KnockoutObservable<number>
    lower_range: KnockoutObservable<number>
    upper_range: KnockoutObservable<number>
    children: KnockoutObservableArray<number>
    is_edit: KnockoutObservable<boolean>
    editable: KnockoutObservable<EditableFactoryViewModel>

    constructor(factory?: Factory, is_edit: boolean = false) {
        if (!factory) {
            factory = {
                id: null,
                correlation_id: 0,
                name: "",
                created_at: new Date(0),
                num_children: 1,
                lower_range: 0,
                upper_range: 0,
                children: <[number]> []
            };
        }

        this.id = ko.observable(factory.id);
        this.correlation_id = ko.observable(Math.random() + "");
        this.created_at = ko.observable(new Date(factory.created_at || 0));
        this.name = ko.observable(factory.name || "");
        this.num_children = ko.observable(factory.num_children || 1);
        this.lower_range = ko.observable(factory.lower_range || 0);
        this.upper_range = ko.observable(factory.upper_range || 0);
        this.children = ko.observableArray(factory.children || []);
        this.is_edit = ko.observable(is_edit);
        this.editable = ko.observable(null);
    }

    public save_changes(websocket: WebSocket): boolean {
        // Ensure the editable factory is valid before making any changes.
        if (!this.editable().validate()) {
            return false;
        }

        this.name(this.editable().name());
        this.num_children(this.editable().num_children());
        this.lower_range(this.editable().lower_range_num());
        this.upper_range(this.editable().upper_range_num());

        // Turns off the editable view. The order here is important.
        // We want to hide the view before changing the value;
        this.is_edit(false);
        this.editable(null);

        // Construct the appropriate message based on whether we were
        // pre-existing or not.
        let method: FromBrowser;
        let data: NewFactory | UpdateFactory;
        if (this.id()) {
            method = FromBrowser.Update;
            data = {
                id: this.id(),
                name: this.name(),
                num_children: this.num_children(),
                lower_range: this.lower_range(),
                upper_range: this.upper_range()
            };
        } else {
            method = FromBrowser.New;
            data = {
                correlation_id: this.correlation_id(),
                name: this.name(),
                num_children: this.num_children(),
                lower_range: this.lower_range(),
                upper_range: this.upper_range()
            };
        }

        let message: Message = {
            method: method,
            data: data
        };

        websocket.send(JSON.stringify(message));
        return true;
    }

    // ui bound
    cancel_changes() {
        // The order here is important. We want to hide the view
        // before changing any of the parameters.
        this.is_edit(false);
        this.editable(null);
    }

    // ui bound
    edit() {
        // If this wasn't set then existing edits could be thrown away
        // if this is triggered twice.
        if (this.is_edit()) {
            return;
        }

        let editable_factory = new EditableFactoryViewModel(
            this.id(),
            this.name(),
            this.num_children(),
            this.lower_range(),
            this.upper_range());

        // The order here is important. We want the fields all set
        // before we make the field visible.
        this.editable(editable_factory);
        this.is_edit(true);
    }

    // Returns true if update went through, false otherwise
    public update_from(factory: Factory, replace_edit: boolean): boolean {
        if (!replace_edit && this.is_edit()) {
            // The user is still editing, we need to allow
            // them to make the choice of accepting the update
            // or overriding it with their current edit.
            return false;
        }

        this.is_edit(false);
        if (factory.name) { this.name(factory.name); }
        if (factory.num_children) { this.num_children(factory.num_children); }
        if (factory.lower_range) { this.lower_range(factory.lower_range); }
        if (factory.upper_range) { this.upper_range(factory.upper_range); }
        if (factory.children) { this.children(factory.children); }
        return true;
    }
}