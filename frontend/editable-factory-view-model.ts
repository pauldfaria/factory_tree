import * as ko from "knockout";

/*
    This class is used to represent a factory
    while it's being editted by the user.
    It stores the 
*/
export class EditableFactoryViewModel {
    id: KnockoutObservable<string>
    name: KnockoutObservable<string>
    num_children: KnockoutObservable<number>
    lower_range: KnockoutObservable<string>
    upper_range: KnockoutObservable<string>
    lower_range_num: KnockoutComputed<number>
    upper_range_num: KnockoutComputed<number>
    has_name_error: KnockoutObservable<boolean>
    has_range_error: KnockoutObservable<boolean>
    has_error: KnockoutComputed<boolean>

    constructor(id: string, name: string, num_children: number, lower_range: number, upper_range: number) {
        this.id = ko.observable(id);
        this.name = ko.observable(name || "");
        this.num_children = ko.observable(num_children || 1);
        this.lower_range = ko.observable((lower_range || 0) + "");
        this.upper_range = ko.observable((upper_range || 0) + "");
        this.has_name_error = ko.observable(false);
        this.has_range_error = ko.observable(false);

        this.lower_range_num = ko.computed(() => parseInt(this.lower_range(), 10));
        this.upper_range_num = ko.computed(() => parseInt(this.upper_range(), 10));

        this.has_error = ko.computed(() =>
            this.has_name_error() || this.has_range_error());

        // Subscriptions to clear out error messages on ui when
        // values fixed. Helps the user know they've fixed the problem.
        this.name.subscribe(() => {
            if (this.has_error()) {
                this.validate_name();
            }
        });
        this.lower_range.subscribe(() => {
            if (this.has_error()) {
                this.validate_range();
            }
        });
        this.upper_range.subscribe(() => {
            if (this.has_error()) {
                this.validate_range();
            }
        });
    }

    public validate(): boolean {
        // Don't somplify into a single expression because
        // we want all of the validations to be run.
        let name_ok = this.validate_name();
        let range_ok = this.validate_range();
        return  name_ok && range_ok;
    }

    validate_name(): boolean {
        // Name is required
        if (this.name().trim().length === 0) {
            this.has_name_error(true);
            return false;
        }

        this.has_name_error(false);
        return true;
    }

    validate_range(): boolean {
        // Lower range must be a number greater than or equal
        // to 0 and less than or equal to upper range.
        if (isNaN(this.lower_range_num())
            || isNaN(this.upper_range_num())
            || this.lower_range_num() > this.upper_range_num()) {
            this.has_range_error(true);
            return false;
        }

        this.has_range_error(false);
        return true;
    }
}