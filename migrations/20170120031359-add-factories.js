'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.runSql('CREATE EXTENSION "uuid-ossp"')
    .then(() => db.runSql(
      'CREATE TABLE factories ( \
        id UUID PRIMARY KEY DEFAULT uuid_generate_v1mc(), \
        correlation_id VARCHAR NOT NULL, \
        created_at TIMESTAMP with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, \
        name VARCHAR NOT NULL, \
        lower_range INT NOT NULL, \
        upper_range INT NOT NULL, \
        num_children INT NOT NULL, \
        children INT[] NOT NULL \
      )'));
};

exports.down = function(db) {
  return db.dropTable('factories')
    .then(() => db.runSql('DROP EXTENSION "uuid-ossp"'));
};

exports._meta = {
  "version": 1
};
