import { Server, OPEN } from "ws";
import { Message, FromServer } from "../dtos/message";

export class WebSocketServer extends Server {
    broadcast(message: Message, options?: {mask?: boolean, binary?: boolean}) {
        console.log("WS => " + FromServer[message.method]);
        let response = JSON.stringify(message);
        this.clients.forEach(client => {
            if (client.readyState == OPEN) {
                client.send(response, options);
            }
        });
    }
}