import { Server } from "http";
import { WebSocketServer } from "./websocket-extension";
import { Message, FromBrowser, FromServer } from "../dtos/message";
import { Factory } from "../dtos/factory";
import { FactoryService } from "./domain/factory-service";
import { Pool } from "pg";
import { Config as DbConfig } from "pg-connection-string";

export function WebSocketFor(server: Server, dbConfig: DbConfig) {
    let wss = new WebSocketServer({server: server});
    console.log("websocket server created");

    let config = {
        user: dbConfig.user,
        database: dbConfig.database,
        password: dbConfig.password,
        ssl: dbConfig.ssl,
        host: dbConfig.host,
        port: parseInt(dbConfig.port, 10),
        max: 10,
        idleTimeoutMillis: 30000,
    };
    let pool = new Pool(config);
    let factoryService = new FactoryService(pool);

    wss.on("connection", (ws) => {
        console.log("websocket connection open to: %s => %s:%s", ws.protocol, ws.upgradeReq.connection.remoteAddress, ws.upgradeReq.connection.remotePort);
        factoryService.getFactories()
            .then(factories => {
                let message: Message = {
                    method: FromServer.Initial,
                    data: factories
                };
                ws.send(JSON.stringify(message));
            })
            .catch(err => console.error("Failed to fetch initial list of factories: %j", err));

        ws.on("ping", (data: any) => {
            // echo for keepalive
            ws.pong(data);
        });

        ws.on("message", (data: any, flags?: { binary: boolean }) => {
            if (flags && flags.binary) {
                console.warn("Received unexpected binary payload");
                return;
            }

            let message: Message = JSON.parse(data);
            if (!message) {
                console.warn("Received empty message");
                return;
            } else {
                console.log("WS <= " + FromBrowser[message.method]);
            }

            switch (message.method) {
                // Respond immediately to pings to keep the server
                // running while clients are still listening
                case FromBrowser.Ping: {
                    let reply = {
                        method: FromServer.Pong,
                        data: message.data
                    };
                    ws.send(JSON.stringify(reply));
                    break;
                }
                case FromBrowser.New: {
                    factoryService.createFactory(message.data)
                        .then(factory => {
                            let reply = {
                                method: FromServer.Created,
                                data: factory
                            };
                            wss.broadcast(reply);
                        })
                        .catch(err => console.error("Error creating factory: %j", err));
                    break;
                }
                case FromBrowser.Update: {
                    factoryService.updateFactory(message.data)
                        .then(factory => {
                            let reply = {
                                method: FromServer.Updated,
                                data: factory
                            };
                            wss.broadcast(reply);
                        })
                        .catch(err => console.error("Error updating factory: %j", err));
                    break;
                }
                case FromBrowser.Refresh: {
                    factoryService.refreshFactory(message.data)
                        .then(factory => {
                            let reply = {
                                method: FromServer.Updated,
                                data: factory
                            };
                            wss.broadcast(reply);
                        })
                        .catch(err => {
                            console.log("Got to error");
                            console.error("Error refreshing factory: %j", err);
                        });
                    break;
                }
                case FromBrowser.Delete: {
                    factoryService.deleteFactory(message.data)
                        .then(id => {
                            let reply = {
                                method: FromServer.Deleted,
                                data: id
                            };
                            wss.broadcast(reply);
                        })
                        .catch(err => console.error("Error deleting factory: %j", err));
                    break;
                }
                default: {
                    console.warn("Unexpected message method:\n%j", message);
                    break;
                }
            }
        });

        ws.on("close", () => {
            console.log("websocket connection close");
        });
    });
}