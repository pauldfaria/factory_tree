export interface FactoryWithRanges {
    lower_range: number,
    upper_range: number,
    num_children: number
}

export interface ValidatableFactory extends FactoryWithRanges {
    name: string,
}

export function validate(factory: ValidatableFactory): Promise<void> {
    if (!factory.name) {
        return Promise.reject("Invalid name. Name is required")
    }

    if (factory.lower_range > factory.upper_range) {
        return Promise.reject("Invalid range. Lower range must be less than or equal to the upper range");
    }

    if (factory.num_children < 0 || factory.num_children > 15) {
        return Promise.reject("Invalid number of children. Must be between 0 and 15");
    }

    return Promise.resolve();
}

export function generate_children(factory: FactoryWithRanges): [number] {
    let children = <[number]>[];
    for (let i = 0; i < factory.num_children; ++i) {
        children.push(Math.floor(
            Math.random()
            * (factory.upper_range - factory.lower_range + 1)
            + factory.lower_range));
    }

    return children;
}