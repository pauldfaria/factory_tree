import { validate, generate_children } from "./factory-validator";
import { Factory } from "../../dtos/factory";
import { NewFactory } from "../../dtos/new-factory";
import { UpdateFactory } from "../../dtos/update-factory";
import { RefreshFactory } from "../../dtos/refresh-factory";
import { DeleteFactory } from "../../dtos/delete-factory";
import { Pool, Client, QueryResult } from "pg";
import { query, querySingle } from "./client-promise-adapter";
import * as uuid from "uuid";

export class FactoryService {
    pool: Pool

    constructor(pool: Pool) {
        this.pool = pool;
    }

    async getFactories(): Promise<[Factory]> {
        let client = await this.pool.connect();
        try {
            let result = await client.query(
                "SELECT \
                    id,\
                    correlation_id,\
                    created_at,\
                    name,\
                    lower_range,\
                    upper_range,\
                    num_children,\
                    children\
                FROM factories\
                ORDER BY created_at DESC");
            return result.rows as [Factory];
        }
        catch (err) {
            console.warn("Failed to read factories from the db: %j", err);
        }
        finally {
            client.release();
        }
    }

    async createFactory(factory: NewFactory): Promise<Factory> {
        if (factory.name) {
            factory.name = factory.name.trim();
        }

        await validate(factory);
        let children = generate_children(factory);
        let client = await this.pool.connect();
        try {
            return await querySingle<Factory>(client, {
                name: "insert_new",
                text:
                    "INSERT INTO factories(\
                        correlation_id,\
                        name,\
                        lower_range,\
                        upper_range,\
                        num_children,\
                        children) \
                    VALUES($1, $2, $3, $4, $5, $6) \
                    RETURNING \
                        id,\
                        correlation_id,\
                        created_at, \
                        name,\
                        lower_range,\
                        upper_range,\
                        num_children,\
                        children",
                values: [
                    factory.correlation_id,
                    factory.name,
                    factory.lower_range,
                    factory.upper_range,
                    factory.num_children,
                    children
                ]
            });
        }
        catch (err) {
            console.warn("Failed to insert new factory: %j", err);
        }
        finally {
            client.release();
        }
    }

    async updateFactory(factory: UpdateFactory): Promise<Factory> {
        if (factory.name) {
            factory.name = factory.name.trim();
        }

        await validate(factory);
        let client = await this.pool.connect();
        try {
            let result = await querySingle<{no_change: boolean}>(client, {
                name: "update_check",
                text:
                    "SELECT \
                        (lower_range = $2 \
                            AND upper_range = $3 \
                            AND num_children = $4) \
                        AS no_change \
                    FROM factories \
                    WHERE id = $1",
                values: [
                    factory.id,
                    factory.lower_range,
                    factory.upper_range,
                    factory.num_children
                ]
            });

            if (result.no_change) {
                // null here signifies no changes made,
                // so no need to broadcast any changes, and no need to write to DB
                return null;
            }

            let children = generate_children(factory);
            try {
                return await querySingle<Factory>(client, {
                    name: "update",
                    text:
                    "UPDATE factories \
                    SET lower_range = $2, \
                        upper_range = $3, \
                        num_children = $4, \
                        children = $5 \
                    WHERE id = $1\
                    RETURNING \
                        id,\
                        correlation_id,\
                        created_at, \
                        name,\
                        lower_range,\
                        upper_range,\
                        num_children,\
                        children",
                    values: [
                        factory.id,
                        factory.lower_range,
                        factory.upper_range,
                        factory.num_children,
                        children
                    ]
                });
            }
            catch (err) {
                console.warn("Failed to check changes on factory: %j", err);
            }
        }
        catch (err) {
            console.warn("Failed to check changes on factory: %j", err);
        }
        finally {
            client.release();
        }
    }

    async refreshFactory(factory: RefreshFactory): Promise<Factory> {
        let client = await this.pool.connect();
        try {
            let result = await querySingle<{lower_range: number, upper_range: number, num_children: number}>(client, {
                name: "refresh_read",
                text:
                    "SELECT \
                        lower_range, \
                        upper_range, \
                        num_children \
                    FROM factories \
                    WHERE id = $1",
                values: [factory.refresh_id]
            });
            let generator = {
                lower_range: result.lower_range,
                upper_range: result.upper_range,
                num_children: result.num_children
            };
            let children = generate_children(generator);
            try {
                return await querySingle<Factory>(client, {
                    name: "refresh_write",
                    text:
                        "UPDATE factories \
                        SET children = $2 \
                        WHERE id = $1 \
                        RETURNING \
                            id,\
                            correlation_id,\
                            created_at, \
                            name,\
                            lower_range,\
                            upper_range,\
                            num_children,\
                            children",
                    values: [
                        factory.refresh_id,
                        children
                    ]
                });
            }
            catch (err) {
                console.warn("Failed to write children for refresh: %j", err);
            }
        }
        catch (err) {
            console.warn("Failed to read factory for refresh: %j", err);
        }
        finally {
            client.release();
        }
    }

    async deleteFactory(factory: DeleteFactory): Promise<string|null> {
        let client = await this.pool.connect();
        try {
            await query<{id: string}|null>(client, {
                    name: "delete",
                    text:
                        "DELETE FROM factories \
                        WHERE id = $1",
                    values: [factory.delete_id]
            });
            return factory.delete_id;
        }
        catch (err) {
            console.warn("Failed to delete factory: %j", err);
        }
        finally {
            client.release();
        }
    }
}