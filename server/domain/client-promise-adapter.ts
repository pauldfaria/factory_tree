import { Client, QueryConfig } from "pg";

export function querySingle<T>(client: Client, config: QueryConfig): Promise<T> {
    return new Promise<T>((resolve, reject) => {
        client.query(config, (err, result) => {
            if (err) {
                reject(err);
                return;
            }

            if (0 === result.rowCount) {
                reject("Item not found");
                return;
            }

            resolve(result.rows[0] as T);
        });
    });
}

export function query<T>(client: Client, config: QueryConfig): Promise<[T]> {
    return new Promise<[T]>((resolve, reject) => {
        client.query(config, (err, result) => {
            if (err) {
                reject(err);
                return;
            }

            resolve(result.rows as [T]);
        });
    });
}