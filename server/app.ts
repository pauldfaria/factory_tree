import { RunServer } from "./webserver";
import { WebSocketFor } from "./websocket";
import { parse } from "pg-connection-string";

process.on('uncaughtException', (err: Error) =>
    console.error("ERROR: Uncaught exception: " + err));
process.on('unhandledRejection', (reason: string, p: Promise<any>) =>
    console.error("ERROR: Unhandled rejection at: Promise ", p, "reason: ", reason));
process.on('warning', (warning: Error) => {
    console.warn(warning.name);
    console.warn(warning.message);
    console.warn(warning.stack);
});

let port = process.env.PORT || 8080;
let server = RunServer(port);

let dbUrl = process.env.DATABASE_URL;
let dbConfig = parse(dbUrl);

WebSocketFor(server, dbConfig);
