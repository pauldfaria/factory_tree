import * as http from "http";
import * as express from "express";
import * as ejs from "ejs";
import * as fs from "fs";
import * as path from "path";

export function RunServer(port: string) : http.Server {
    let app = express();
    let router = express.Router();

    // simple index route
    router.get("/", (_, res) => {
        res.render('index', {
            max_children: 15
        });
    });

    app.set("view engine", "ejs");
    app.set("views", path.join(process.env.PWD, "/views"));
    if (process.env.DEBUG) {
        // Request logger
        app.use("*", (req, _, next) => {
            console.log(req.connection.remoteAddress + " => /" + req.method + ": " + req.url);
            next();
        });
    }
    app.use("/static/styles", express.static(path.join(process.env.PWD, "public/styles")));
    app.use("/static/scripts/dtos", express.static(path.join(process.env.PWD, "dist/web/dtos")))
    app.use("/static/scripts",
        express.static(path.join(process.env.PWD, "public/scripts")),
        express.static(path.join(process.env.PWD, "dist/web/frontend")));
    app.use("/", router);

    // catch 404 and forward to error handler
    app.use((req, res, next) => {
        var err: any = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    let server = http.createServer(app);
    server.listen(port);
    console.log("http server listening on %d", port);

    return server;
}